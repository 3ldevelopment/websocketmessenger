package com.example.demo.model.dto;

public record GreetingRequest(String name) {
}
