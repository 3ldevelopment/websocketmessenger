package com.example.demo.model.dto;

public record GreetingResponse(String message) {
}
